﻿using System.Collections.Generic;

namespace ElevatorCentral
{
    /// <summary>
    /// Elevator
    /// </summary>
    public class Elevator
    {
        public int ID { get; }
        public int FloorNum { get; }
        public bool IsMoving => Direction == Directions.None;
        public Directions Direction { get; set; }
        public int CurrentFloor { get; set; }
        public List<int> DestinationFloors { get; }

        /// <summary>
        /// Constructor. Initialize properties to default values.
        /// </summary>
        /// <param name="id">Elevator id.</param>
        /// <param name="floorNum">Number of floors.</param>
        public Elevator(int id, int floorNum)
        {
            ID = id;
            FloorNum = floorNum;
            Direction = Directions.None;
            CurrentFloor = 1;
            DestinationFloors = new List<int>();
        }

        /// <summary>
        /// Add destination floor to list. Checks if floor is not already in list.
        /// </summary>
        /// <param name="destinationFloor"></param>
        public void AddDestinationFloor(int destinationFloor)
        {
            if (!DestinationFloors.Contains(destinationFloor))
            {
                DestinationFloors.Add(destinationFloor);
            }
        }
    }

    /// <summary>
    /// Directions that elevator is moving to. Directions.None is used when elevator has stopped.
    /// </summary>
    public enum Directions { None, Up, Down }
}
