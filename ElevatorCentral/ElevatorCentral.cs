﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ElevatorCentral
{
    //TODO: make thread safe addition of new request (use locking or cuncurrent collection)
    /// <summary>
    /// Elevator central controls work of elevators.
    /// </summary>
    public class ElevatorCentral
    {
        public Action<string> Logger { get; set; }
        private List<Elevator> ElevatorList { get; set; }
        private List<ElevatorRequest> ProcessedRequestList { get; set; }
        private Queue<ElevatorRequest> NewRequestsQueue { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="elevatorList">List of elevators</param>
        public ElevatorCentral(List<Elevator> elevatorList)
        {
            ElevatorList = elevatorList;
            ProcessedRequestList = new List<ElevatorRequest>();
            NewRequestsQueue = new Queue<ElevatorRequest>();
        }

        /// <summary>
        /// Main method to process state of elevators
        /// </summary>
        public void Process()
        {
            foreach (var elevator in ElevatorList)
            {
                ProcessDestinationFloors(elevator);
                ProcessRelevantRequests(elevator);
                MoveElevator(elevator);

                if (!elevator.IsMoving && elevator.DestinationFloors.Count > 0)
                {
                    StartMovingElevator(elevator);
                }
            }

            ProcessNewRequests();

        }

        //TODO: Use locking for thread safe or use concurrent queue
        /// <summary>
        /// Add new request to queue
        /// </summary>
        /// <param name="request"></param>
        public void AddNewRequest(ElevatorRequest request)
        {
            NewRequestsQueue.Enqueue(request);
        }

        /// <summary>
        /// Check if any active request is processed by elevators
        /// </summary>
        /// <returns>True if there is any active request processed by elevators</returns>
        public bool IsAnyActiveRequest()
        {
            return ElevatorList.Any(e => e.DestinationFloors.Count > 0);
        }

        private void ProcessDestinationFloors(Elevator elevator)
        {
            if (elevator.DestinationFloors.Contains(elevator.CurrentFloor))
            {
                elevator.DestinationFloors.Remove(elevator.CurrentFloor);
                Logger($"Elevator [{elevator.ID}] removing floor [{elevator.CurrentFloor}] from destination list");
            }

            if (elevator.IsMoving && elevator.DestinationFloors.Count == 0)
            {
                elevator.Direction = Directions.None;
                Logger($"Elevator [{elevator.ID}] is stoping at floor [{elevator.CurrentFloor}] due to empty destination list");
            }
        }

        private void ProcessRelevantRequests(Elevator elevator)
        {
            List<ElevatorRequest> relevantRequests = ProcessedRequestList
                                                        .Where(r => r.ElevatorId.Equals(elevator.ID))
                                                        .Where(r => r.FromFloor.Equals(elevator.CurrentFloor))
                                                        .ToList();

            if (relevantRequests.Count > 0)
            {
                foreach (var request in relevantRequests)
                {
                    elevator.AddDestinationFloor(request.ToFloor);
                    Logger($"Elevator [{elevator.ID}] adding destination floor [{request.ToFloor}] for request [{request.ID}]");
                    
                    ProcessedRequestList.Remove(request);
                    Logger($"Request [{request.ID}] removing from list");
                }
            }
        }

        private void MoveElevator(Elevator elevator)
        {
            if (elevator.IsMoving)
            {
                if (elevator.Direction == Directions.Up)
                {
                    elevator.CurrentFloor++;
                }
                else if (elevator.Direction == Directions.Down)
                {
                    elevator.CurrentFloor--;
                }

                Logger($"Elevator [{elevator.ID}] moving to floor [{elevator.CurrentFloor}]");

                if (elevator.CurrentFloor == 0 || elevator.CurrentFloor == elevator.FloorNum)
                {
                    elevator.Direction = Directions.None;
                    Logger($"Elevator [{elevator.ID}] stopping at floor [{elevator.CurrentFloor}]");
                }
            }
        }

        private void StartMovingElevator(Elevator elevator)
        {
            if (elevator.CurrentFloor < elevator.DestinationFloors.Min())
            {
                elevator.Direction = Directions.Up;
                Logger($"Elevator [{elevator.ID}] started moving up");
            }
            else
            {
                elevator.Direction = Directions.Down;
                Logger($"Elevator [{elevator.ID}] started moving down");
            }
        }

        private void ProcessNewRequests()
        {
            while (NewRequestsQueue.Count > 0)
            {
                ElevatorRequest newRequest = NewRequestsQueue.Dequeue();

                Logger($"Processing request [{newRequest.ID}]");

                AssignRequestToElevator(newRequest);

                ProcessedRequestList.Add(newRequest);
            }
        }

        private void AssignRequestToElevator(ElevatorRequest newRequest)
        {
            Elevator elevator = null;
            List<Elevator> relevantElevatorList = ElevatorList
                                .Where(e => e.IsMoving)
                                .Where(e => (e.Direction == Directions.Up && e.CurrentFloor <= newRequest.FromFloor && newRequest.Direction == Directions.Up) ||
                                            (e.Direction == Directions.Down && e.CurrentFloor >= newRequest.FromFloor && newRequest.Direction == Directions.Down))
                                .ToList();

            if (relevantElevatorList.Count > 0)
            {
                int min = relevantElevatorList.Min(e => Math.Abs(e.CurrentFloor - newRequest.FromFloor));
                elevator = relevantElevatorList.Where(e => Math.Abs(e.CurrentFloor - newRequest.FromFloor) == min).FirstOrDefault();
            }

            if (elevator == null)
            {
                relevantElevatorList = ElevatorList.Where(e => !e.IsMoving).ToList();
                if (relevantElevatorList.Count > 0)
                {
                    int min = relevantElevatorList.Min(e => Math.Abs(e.CurrentFloor - newRequest.FromFloor));
                    elevator = relevantElevatorList.Where(e => Math.Abs(e.CurrentFloor - newRequest.FromFloor) == min).FirstOrDefault(); 
                }
            }

            if (elevator == null)
            {
                elevator = ElevatorList.FirstOrDefault();
            }

            newRequest.ElevatorId = elevator.ID;
            elevator.DestinationFloors.Add(newRequest.FromFloor);

            Logger($"Request [{newRequest.ID}] assigned to elevator [{elevator.ID}]");
        }
    }
}
