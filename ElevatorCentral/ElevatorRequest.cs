﻿namespace ElevatorCentral
{
    /// <summary>
    /// Request for elevator
    /// </summary>
    public class ElevatorRequest
    {
        public int ID { get; set; }
        public int FromFloor { get; set; }
        public int ToFloor { get; set; }
        public int ElevatorId { get; set; }
        public Directions Direction { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Request id</param>
        /// <param name="fromFloor">Request from floor</param>
        /// <param name="toFloor">Request to floor</param>
        public ElevatorRequest(int id, int fromFloor, int toFloor)
        {
            ID = id;
            FromFloor = fromFloor;
            ToFloor = toFloor;
            Direction = fromFloor < toFloor ? Directions.Up : Directions.Down;
        }
    }
}
