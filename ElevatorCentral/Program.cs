﻿using System;

namespace ElevatorCentral
{
    /* TODO: 1. Add input validation and null checks
     *       2. Add exception / error handling
     *       3. Implement proper logging
     *       4. GUI to display simulation state?
     *       5. Extend Elevator (type of elevator, apply physical model, capacity, load configuration from file)
     *       5. Implement time based and / or event based simulation
     *       6. Implement generation of requests from JSON file
     *       7. ElevatorCentral processing rules from configuration file
     */
    class Program
    {
        private const int FloorNum = 4;
        private const int ElevatorNum = 2;
        private const int ElevatorRequestMax = 20;

        static void Main(string[] args)
        {
            Simulation simulation = new Simulation(FloorNum, ElevatorNum, ElevatorRequestMax);
            simulation.Initialize();
            simulation.Run();
            Console.ReadLine();
        }
    }
}
