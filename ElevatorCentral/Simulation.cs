﻿using System;
using System.Collections.Generic;

namespace ElevatorCentral
{
    /// <summary>
    /// Class to simulate work of elevator central
    /// </summary>
    public class Simulation
    {
        public Action<string> Logger = Console.WriteLine;
        public int FloorNum { get; set; }
        public int ElevatorNum { get; set; }
        public int ElevatorRequestMax { get; set; }
        public int RequestNum { get; set; }
        public ElevatorCentral ElevatorCentral { get; set; }
        public Random RNG { get; set; }

        public Simulation(int floorNum, int elevatorNum, int requestMax)
        {
            FloorNum = floorNum;
            ElevatorNum = elevatorNum;
            ElevatorRequestMax = requestMax;
            RequestNum = 0;
        }

        //TODO: consider moving part of code to constructor
        /// <summary>
        /// Initialize simulation.
        /// </summary>
        public void Initialize()
        {
            Logger("Initializing simulation:");
            Logger("Number of elevators:\t" + ElevatorNum);
            Logger("Number of floors:\t" + FloorNum);
            Logger("");

            List<Elevator> elevatorList = new List<Elevator>();

            for (int i = 0; i < ElevatorNum; i++)
            {
                elevatorList.Add(new Elevator(i, FloorNum));
            }

            ElevatorCentral = new ElevatorCentral(elevatorList);
            ElevatorCentral.Logger = Logger;
            RNG = new Random();
        }

        /// <summary>
        /// Run simulation
        /// </summary>
        public void Run()
        {
            // loop breaks when number of requests is equal to defined maximum number of requests
            // and elevators has finished processing of all active requests
            while (true)
            {
                ElevatorRequest request = GenerateNewRequest();

                if (request != null)
                {
                    ElevatorCentral.AddNewRequest(request);
                }

                ElevatorCentral.Process();

                if (RequestNum >= ElevatorRequestMax && ElevatorCentral.IsAnyActiveRequest())
                {
                    break;
                }
            }
        }

        private ElevatorRequest GenerateNewRequest()
        {
            ElevatorRequest request = null;

            if (RequestNum >= ElevatorRequestMax)
            {
                return request;
            }

            // For testing sake random number generator is used to generate request.
            // Value in if condition controls probability of generation of request.
            if (RNG.Next(10) > 4)
            {
                RequestNum++;

                int fromFloor = RNG.Next(FloorNum) + 1;
                int toFloor = fromFloor;
                while (toFloor == fromFloor)
                {
                    toFloor = RNG.Next(FloorNum) + 1;
                }

                request = new ElevatorRequest(RequestNum, fromFloor, toFloor);

                Logger($"New request generated [ID:{RequestNum}, From floor:{fromFloor}, To floor:{toFloor}]");
            }

            return request;           
        }
    }
}
